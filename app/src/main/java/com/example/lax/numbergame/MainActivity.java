package com.example.lax.numbergame;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.util.Log;

import java.util.Random;


public class MainActivity extends ActionBarActivity {

    int numberToGuess;
    int userNumber;

    int attempts = 0;
    boolean gameStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void newGame(View view) {
        TextView tvInfo = (TextView)findViewById(R.id.textViewInfo);
        tvInfo.setText("1-100");
        TextView tvAttempts = (TextView)findViewById(R.id.textViewAttempts);
        tvAttempts.setVisibility(View.VISIBLE);

        this.setNumberToGuess(this.randInt(1,100));
        this.setAttempts(0);
        this.runGame();


    }

    private void setNumberToGuess(int number) {
        this.numberToGuess = number;
    }

    private void runGame() {
        this.gameStarted = true;
    }
    private void stopGame() {
        this.gameStarted = false;
    }
    private int getAttempts() { return this.attempts; }
    private void setAttempts(int attempts) {
        TextView tvAttempts = (TextView)findViewById(R.id.textViewAttempts);

        this.attempts = attempts;
        tvAttempts.setText("Attempts: " + this.attempts);
    }

    public void takeDigest(View view) {
        if (!this.gameStarted) {
            this.newGame(view);
        }

        String message;
        TextView tvInfo = (TextView)findViewById(R.id.textViewInfo);
        EditText etUserNumber = (EditText)findViewById(R.id.textboxUserNumber);

        this.setAttempts(this.getAttempts() + 1);

        Log.v("test", "Parsing: " + etUserNumber.getText().toString());

        userNumber = Integer.parseInt(etUserNumber.getText().toString());

        if (userNumber > numberToGuess) {
            message = "LESS";
            etUserNumber.setText("");
        } else if ( userNumber < numberToGuess) {
            message = "BIGGER";
            etUserNumber.setText("");
        } else {
            message = "You won!";
        }

        tvInfo.setText(message);

//        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
//        toast.show();

    }

    public int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
